# my todo list

A little project on django to do a simple todo list

 - A little overview of Django framework
 - Some simple templates
 - Some simple APIs
 - A little project structure
 - (Optional) small testing trials
 - (Optional) play a little bit with turbolinks and other cool frameworks
 - (Optional) multiple languages management